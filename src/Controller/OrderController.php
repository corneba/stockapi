<?php

// src/Controller/OrderController.php

namespace App\Controller;

use App\Repository\CustomerRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Repository\InventoryRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Lock\LockInterface;
use Symfony\Component\Lock\Exception\LockAcquiringException;
use Symfony\Component\Lock\Exception\LockConflictedException;


class OrderController {

    private $orderRepository;
    private $productRepository;
    private $customerRepository;
    private $inventoryRepository;

    public function __construct(OrderRepository $orderRepository, ProductRepository $productRepository, CustomerRepository $customerRepository, InventoryRepository $inventoryRepository) {
        $this->orderRepository = $orderRepository;
        $this->customerRepository = $customerRepository;
        $this->productRepository = $productRepository;
        $this->inventoryRepository = $inventoryRepository;
    }

    /**
     * @Route("/order/add", name="add_order", methods={"POST"})
     */
    public function add(LockInterface $lock, Request $request): JsonResponse {

        $data = json_decode($request->getContent(), true);

        $product_id = $data['product_id'];
        $price = $data['price'];
        $product_name = $data['product_name'];
        $customer_id = $data['customer_id'];

        if (empty($product_id) || empty($price) || empty($product_name) || empty($customer_id)) {
            throw new NotFoundHttpException('product_id, price, product_name and customer_id are mandatory fields');
        }

        $inventory = $this->inventoryRepository->findOneBy(["product_id" => $product_id]);
        if ($inventory && $inventory->getQty() > 0) {
            // deal with concurrency
            try {
                $lock->acquire(true);
                // accept order
                $this->orderRepository->addOrder($product_id, $price, $product_name, $customer_id);

                // update inventory
                $inventory->setQty($inventory->getQty() - 1);
                $this->inventoryRepository->editInventory($inventory);

                //release lock
                $lock->release();
            } catch (LockConflictedException $e) {
                return new JsonResponse(['status' => 'something went wrong, please retry'], Response::HTTP_OK);
            } catch (LockAcquiringException $e) {
                return new JsonResponse(['status' => 'something went wrong, please retry'], Response::HTTP_OK);
            }
        } else {
            return new JsonResponse(['status' => 'this product is not in stock anymore'], Response::HTTP_OK);
        }


        return new JsonResponse(['status' => 'order successfully added'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/order/{id}", name="get_order", methods={"GET"})
     */
    public function get($id): JsonResponse {
        $order = $this->orderRepository->findOneBy(['id' => $id]);

        if ($order) {
            $data = [
                'id' => $order->getId(),
                'product_id' => $order->getProductId(),
                'price' => $order->getPrice(),
                'product_name' => $order->getProductName(),
                'customer_id' => $order->getCustomerId()
            ];
        } else {
            return new JsonResponse(Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/orders", name="get_all_orders", methods={"GET"})
     */
    public function getAll(): JsonResponse {
        $orders = $this->orderRepository->findAll();
        $data = [];

        foreach ($orders as $order) {
            $data[] = [
                'id' => $order->getId(),
                'product_id' => $order->getProductId(),
                'price' => $order->getPrice(),
                'product_name' => $order->getProductName(),
                'customer_id' => $order->getCustomerId()
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/order/{id}", name="update_order", methods={"PUT"})
     */
    public function update(LockInterface $lock, $id, Request $request): JsonResponse {

        $order = $this->orderRepository->findOneBy(['id' => $id]);
        if ($order) {
            $data = json_decode($request->getContent(), true);

            empty($data['product_id']) ? true : $order->setCode($data['product_id']);
            empty($data['price']) ? true : $order->setName($data['price']);
            empty($data['product_name']) ? true : $order->setDescription($data['product_name']);
            empty($data['customer_id']) ? true : $order->setPrice($data['customer_id']);

            try {
                // deal with concurrency
                $lock->acquire(true);
                $updated = $this->orderRepository->editOrder($order);
                $lock->release();
            } catch (LockConflictedException $e) {
                return new JsonResponse(['status' => 'something went wrong, please retry'], Response::HTTP_OK);
            } catch (LockAcquiringException $e) {
                return new JsonResponse(['status' => 'something went wrong, please retry'], Response::HTTP_OK);
            }
        } else {
            return new JsonResponse(Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($updated->toArray(), Response::HTTP_OK);
    }

    /**
     * @Route("/order/{id}", name="delete_order", methods={"DELETE"})
     */
    public function delete($id): JsonResponse {
        $order = $this->orderRepository->findOneBy(['id' => $id]);
        if ($order) {
            $this->orderRepository->removeOrder($order);
        } else {
            return new JsonResponse(Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(['status' => 'order deleted'], Response::HTTP_OK);
    }

}
