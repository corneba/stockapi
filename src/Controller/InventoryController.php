<?php

// src/Controller/InventoryController.php

namespace App\Controller;

use App\Repository\InventoryRepository;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Lock\LockInterface;
use Symfony\Component\Lock\Exception\LockAcquiringException;
use Symfony\Component\Lock\Exception\LockConflictedException;

class InventoryController {

    private $inventoryRepository;
    private $productRepository;

    public function __construct(InventoryRepository $inventoryRepository, ProductRepository $productRepository) {
        $this->inventoryRepository = $inventoryRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * @Route("/inventory/add", name="add_inventory", methods={"POST"})
     */
    public function add(LockInterface $lock, Request $request): JsonResponse {
        $data = json_decode($request->getContent(), true);

        $product_id = (isset($data['product_id']) ? $data['product_id'] : "");
        $qty = (isset($data['qty']) ? $data['qty'] : "");

        if (empty($product_id) || empty($qty)) {
            throw new NotFoundHttpException('product_id and qty are mandatory fields');
        }

        $product = $this->productRepository->findOneBy(["id" => $product_id]);
        if (!$product) {
            throw new BadRequestException("product id does not exist");
        }

        $inventory = $this->inventoryRepository->findOneBy(['product_id' => $product_id]);

        if (!$inventory) {

        try {
            // deal with concurrency
            $lock->acquire(true);
            $this->inventoryRepository->addInventory($product, $qty);
            //release lock
            $lock->release();
        } catch (LockConflictedException $e) {
            return new JsonResponse(['status' => 'something went wrong, please retry'], Response::HTTP_OK);
        } catch (LockAcquiringException $e) {
            return new JsonResponse(['status' => 'something went wrong, please retry'], Response::HTTP_OK);
        }

        } else {
            return new JsonResponse(['status' => 'inventory for this id already exists, use the inventory/update'], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse(['status' => 'inventory successfully added'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/inventory/{id}", name="get_inventory", methods={"GET"})
     */
    public function get($id): JsonResponse {
        $inventory = $this->inventoryRepository->findOneBy(['id' => $id]);

        if ($inventory) {
            $data = [
                'id' => $inventory->getId(),
                'product' => $inventory->getProductId()->toArray(),
                'qty' => $inventory->getQty()
            ];
        } else {
            return new JsonResponse(Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/inventories", name="get_all_inventory", methods={"GET"})
     */
    public function getAll(): JsonResponse {
        $inventories = $this->inventoryRepository->findAll();
        $data = [];

        foreach ($inventories as $inventory) {
            $data[] = [
                'id' => $inventory->getId(),
                'product' => $inventory->getProductId()->toArray(),
                'qty' => $inventory->getQty()
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/inventory/{id}", name="update_inventory", methods={"PUT"})
     */
    public function update(LockInterface $lock, $id, Request $request): JsonResponse {
        $inventory = $this->inventoryRepository->findOneBy(['id' => $id]);
        if ($inventory) {
            // deal with concurrency
            try {
                $lock->acquire(true);
                $data = json_decode($request->getContent(), true);
                empty($data['qty']) ? true : $inventory->setQty($data['qty']);
                $updated = $this->inventoryRepository->editInventory($inventory);
                $lock->release();
            } catch (LockConflictedException $e) {
                return new JsonResponse(['status' => 'something went wrong, please retry'], Response::HTTP_OK);
            } catch (LockAcquiringException $e) {
                return new JsonResponse(['status' => 'something went wrong, please retry'], Response::HTTP_OK);
            }
        } else {
            return new JsonResponse(Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(['status' => 'inventory updated'], Response::HTTP_OK);
    }

    /**
     * @Route("/inventory/{id}", name="delete_inventory", methods={"DELETE"})
     */
    public function delete($id): JsonResponse {
        $inventory = $this->inventoryRepository->findOneBy(['id' => $id]);
        if ($inventory) {
            $this->inventoryRepository->removeInventory($inventory);
        } else {
            return new JsonResponse(Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(['status' => 'inventory deleted'], Response::HTTP_OK);
    }

}
