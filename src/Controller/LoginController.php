<?php
// src/Controller/LoginController.php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoginController {
    
    private $userRepository;
    
    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;        
    }
    
    /**
     * @Route("/login/tokenRequest", name="login", methods={"POST"})
     */
    public function tokenRequest(Request $request,UserPasswordEncoderInterface $passwordEncoder): JsonResponse {
        
       $data = json_decode($request->getContent(), true);
       if (empty($data["username"]) || empty($data["password"])) {
            throw new NotFoundHttpException('username and password are mandatory fields');
        }
        
       $user = $this->userRepository->findOneBy(["username" => $data["username"]]);
       $validPassword = $passwordEncoder->isPasswordValid($user, $data['password']);
       if ($user && $validPassword) {
           return new JsonResponse(['status' => "ok", 'token' => $user->getApiToken()], Response::HTTP_OK);
       } else {
           return new JsonResponse(['status' => "bad credentials"], Response::HTTP_OK);
       }
    }
}
