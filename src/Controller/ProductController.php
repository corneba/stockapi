<?php

// src/Controller/ProductController.php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ProductController {

    private $productRepository;

    public function __construct(ProductRepository $productRepository) {
        $this->productRepository = $productRepository;
    }

    
    /**
     * @Route("/product/add", name="add_product", methods={"POST"})
     */
    public function add(Request $request): JsonResponse {
        $data = json_decode($request->getContent(), true);

        $code = $data['code'];
        $name = $data['name'];
        $description = $data['description'];
        $price = $data['price'];
        $active = $data['active'];

        if (empty($code) || empty($name) || empty($description) || empty($price) || empty($active)) {
            throw new NotFoundHttpException('code, name, description, price and active are mandatory fields');
        }

        $exists = $this->productRepository->findOneBy(["code" => $code]);
        if ($exists) {
            throw new BadRequestException("product code already exists");
        }

        $this->productRepository->addProduct($code, $name, $description, $price, $active);

        return new JsonResponse(['status' => 'product successfully added'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/product/{id}", name="get_product", methods={"GET"})
     */
    public function get($id): JsonResponse {
        $product = $this->productRepository->findOneBy(['id' => $id]);

        if ($product) {
            $data = [
                'id' => $product->getId(),
                'code' => $product->getCode(),
                'name' => $product->getName(),
                'description' => $product->getDescription(),
                'price' => $product->getPrice(),
                'active' => $product->getActive()
            ];
        } else {
            return new JsonResponse(Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/products", name="get_all_products", methods={"GET"})
     */
    public function getAll(): JsonResponse {
        $products = $this->productRepository->findAll();
        $data = [];

        foreach ($products as $product) {
            $data[] = [
                'id' => $product->getId(),
                'code' => $product->getCode(),
                'name' => $product->getName(),
                'description' => $product->getDescription(),
                'price' => $product->getPrice(),
                'active' => $product->getActive()
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/product/{id}", name="update_product", methods={"PUT"})
     */
    public function update($id, Request $request): JsonResponse {
        $product = $this->productRepository->findOneBy(['id' => $id]);
        if ($product) {
            $data = json_decode($request->getContent(), true);

            empty($data['code']) ? true : $product->setCode($data['code']);
            empty($data['name']) ? true : $product->setName($data['name']);
            empty($data['description']) ? true : $product->setDescription($data['description']);
            empty($data['price']) ? true : $product->setPrice($data['price']);
            empty($data['active']) ? true : $product->setActive((bool) $data['active']);

            $updated = $this->productRepository->editProduct($product);
        } else {
            return new JsonResponse(Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($updated->toArray(), Response::HTTP_OK);
    }

    /**
     * @Route("/product/{id}", name="delete_product", methods={"DELETE"})
     */
    public function delete($id): JsonResponse {
        $product = $this->productRepository->findOneBy(['id' => $id]);
        if ($product) {
            $this->productRepository->removeProduct($product);
        } else {
            return new JsonResponse(Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(['status' => 'product deleted'], Response::HTTP_OK);
    }

}
