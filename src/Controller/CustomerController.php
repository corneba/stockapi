<?php

// src/Controller/CustomerController.php

namespace App\Controller;

use App\Repository\CustomerRepository;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CustomerController {

    private $customerRepository;

    public function __construct(CustomerRepository $customerRepository) {
        $this->customerRepository = $customerRepository;
    }

    
    /**
     * @Route("/customer/add", name="add_customer", methods={"POST"})
     */
    public function add(Request $request): JsonResponse {
        $data = json_decode($request->getContent(), true);

        $firstname = $data['firstname'];
        $lastname = $data['lastname'];
        $email = $data['email'];
        
        if (empty($firstname) || empty($lastname) || empty($email) ) {
            throw new NotFoundHttpException('firstname,lastname and email are mandatory fields');
        }

        $exists = $this->customerRepository->findOneBy(["email" => $email]);
        if ($exists) {
            throw new BadRequestException("customer email already exists");
        }

        $this->customerRepository->addCustomer($firstname, $lastname, $email);

        return new JsonResponse(['status' => 'customer successfully added'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/customer/{id}", name="get_customer", methods={"GET"})
     */
    public function get($id): JsonResponse {
        $customer = $this->customerRepository->findOneBy(['id' => $id]);

        if ($customer) {
            $data = [
                'id' => $customer->getId(),
                'firstname' => $customer->getFirstname(),
                'lastname' => $customer->getLastname(),
                'email' => $customer->getEmail(),                
            ];
        } else {
            return new JsonResponse(Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/customers", name="get_all_customers", methods={"GET"})
     */
    public function getAll(): JsonResponse {
        $customers = $this->customerRepository->findAll();
        $data = [];

        foreach ($customers as $customer) {
            $data[] = [
                'id' => $customer->getId(),
                'firstname' => $customer->getFirstname(),
                'lastname' => $customer->getLastname(),
                'email' => $customer->getEmail()                
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/customer/{id}", name="update_customer", methods={"PUT"})
     */
    public function update($id, Request $request): JsonResponse {
        $customer = $this->customerRepository->findOneBy(['id' => $id]);
        if ($customer) {
            $data = json_decode($request->getContent(), true);

            empty($data['firstname']) ? true : $customer->setFirstname($data['firstname']);
            empty($data['lastname']) ? true : $customer->setLastname($data['lastname']);
            empty($data['email']) ? true : $customer->setEmail($data['email']);
           
            $updated = $this->customerRepository->editCustomer($customer);
        } else {
            return new JsonResponse(Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($updated->toArray(), Response::HTTP_OK);
    }

    /**
     * @Route("/customer/{id}", name="delete_customer", methods={"DELETE"})
     */
    public function delete($id): JsonResponse {
        $customer = $this->customerRepository->findOneBy(['id' => $id]);
        if ($customer) {
            $this->customerRepository->removeCustomer($customer);
        } else {
            return new JsonResponse(Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(['status' => 'customer deleted'], Response::HTTP_OK);
    }

}


